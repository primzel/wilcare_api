package org.wilcare;

/**
 * Created by qasim on 1/17/15.
 */
public class Constents {

    public static final String URL="http://localhost/Root/wilcare_service";

    public static final String GET_ALL_PATIENT_URL="/Patient/index";

    public static final String SAVE_PATIENT_URL="/Patient/insert_patient";

    public static final String GET_ALL_EXAM_LIST="/Exam/index";

    public static final String SAVE_EXAMINATION_DATA="/Exam/insert_exam";

    public static final String GET_ALL_PATIENTS_FOR_EXAM_URL="/Patient/get_all_for_exam_screen";

    public static final String GET_PATIENT_BY_ID_URL="/android/getPatient";

    public static final String GET_ALL_LABS_URL="/android/getLabs";

    public static final String GET_ALL_COUNTRUES_DATA_URL="/Visa/get_country_data";

    public static final String GET_US_STATE_DATA_URL="/android/getUSDATA";

    public static final String GET_ALL_VISA_LIST_URL="/Visa/index";

}
