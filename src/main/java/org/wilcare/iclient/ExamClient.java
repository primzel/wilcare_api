package org.wilcare.iclient;

import org.wilcare.Constents;
import org.wilcare.android.Examination;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

import javax.annotation.PostConstruct;
import java.lang.invoke.ConstantCallSite;
import java.util.List;

/**
 * Created by qasim on 2/11/15.
 */
public interface ExamClient {
    @GET(Constents.GET_ALL_EXAM_LIST)
    List<Examination> getAllExamList();

    @POST(Constents.SAVE_EXAMINATION_DATA)
    String savePatientData(@Body Examination examination);
}
