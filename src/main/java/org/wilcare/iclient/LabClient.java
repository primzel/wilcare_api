package org.wilcare.iclient;

import org.wilcare.Constents;
import org.wilcare.android.Lab;
import retrofit.http.GET;

import java.util.List;

/**
 * Created by qasim on 1/17/15.
 */
public interface LabClient {
    @GET(Constents.GET_ALL_LABS_URL)
    List<Lab> getLabs();
}
