package org.wilcare.iclient;

import org.wilcare.Constents;
import org.wilcare.android.CountryData;
import org.wilcare.android.UsState;
import org.wilcare.android.Visa;
import retrofit.http.GET;

import java.util.List;

/**
 * Created by qasim on 1/17/15.
 */
public interface CountryClient
{
    @GET(Constents.GET_ALL_COUNTRUES_DATA_URL)
    List<CountryData> getCountryData();

    @GET(Constents.GET_US_STATE_DATA_URL)
    List<UsState> getUsStateData();

    @GET(Constents.GET_ALL_VISA_LIST_URL)
    List<Visa> getAllVisaList();
}
