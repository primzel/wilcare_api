package org.wilcare.iclient;

import org.wilcare.Constents;
import org.wilcare.android.Patient;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

import java.util.List;

/**
 * Created by qasim on 1/17/15.
 */
public interface PatientClient {

    @POST(Constents.SAVE_PATIENT_URL)
    String insert_patient(@Body Patient patient);

    @GET(Constents.GET_ALL_PATIENT_URL)
    List<Patient> getAllPatients();

    @GET(Constents.GET_ALL_PATIENTS_FOR_EXAM_URL)
    List<Patient> getAllPatientsForExam();

}
