package org.wilcare.android;

import java.util.List;

/**
 * Created by qasim on 1/17/15.
 */
public class CountryData
{
    private String country;

    private List<State> states;

    public List<State> getStates() {
        return states;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }

    @Override
    public String toString() {
        return "CountryData{" +
                "country='" + country + '\'' +
                ", states=" + states +
                "}";
    }
}
