package org.wilcare.android;

import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

/**
 * Created by qasim on 2/12/15.
 */
public class State {

    private String us_state;

    private List<String> city;

    public String getUs_state() {
        return us_state;
    }

    public void setUs_state(String us_state) {
        this.us_state = us_state;
    }

    public List<String> getCity() {
        return city;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "State{" +
                "us_state='" + us_state + '\'' +
                ", city=" + city +
                '}';
    }
}
