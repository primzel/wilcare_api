package org.wilcare.android;

/**
 * Created by qasim on 1/17/15.
 */
public class Visa
{
    private String visa_id;

    private String visa_type;

    private Boolean default_check;

    public String getVisa_id ()
    {
        return visa_id;
    }

    public void setVisa_id (String visa_id)
    {
        this.visa_id = visa_id;
    }

    public String getVisa_type ()
    {
        return visa_type;
    }

    public void setVisa_type (String visa_type)
    {
        this.visa_type = visa_type;
    }


    public Boolean getDefault_check() {
        return default_check;
    }

    public void setDefault_check(Boolean default_check) {
        this.default_check = default_check;
    }

    @Override
    public String toString() {
        return "Visa{" +
                "visa_id='" + visa_id + '\'' +
                ", visa_type='" + visa_type + '\'' +
                ", default_check=" + default_check +
                '}';
    }
}