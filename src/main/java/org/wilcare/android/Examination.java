package org.wilcare.android;

/**
 * Created by qasim on 2/11/15.
 */
public class Examination
{
    private String exam_place_city;

    private String exam_exp_indicator;

    private String lab_syphillis;

    private String exam_place_country;

    private String exam_date;

    private String screening_site;

    private String panel_physician;

    private String radiology_services;

    private String lab_tb;

    private String patient_id;

    private String exam_time;

    private String exam_exp_date;

    public String getExam_place_city ()
    {
        return exam_place_city;
    }

    public void setExam_place_city (String exam_place_city)
    {
        this.exam_place_city = exam_place_city;
    }

    public String getExam_exp_indicator ()
    {
        return exam_exp_indicator;
    }

    public void setExam_exp_indicator (String exam_exp_indicator)
    {
        this.exam_exp_indicator = exam_exp_indicator;
    }

    public String getLab_syphillis ()
    {
        return lab_syphillis;
    }

    public void setLab_syphillis (String lab_syphillis)
    {
        this.lab_syphillis = lab_syphillis;
    }

    public String getExam_place_country ()
    {
        return exam_place_country;
    }

    public void setExam_place_country (String exam_place_country)
    {
        this.exam_place_country = exam_place_country;
    }

    public String getExam_date ()
    {
        return exam_date;
    }

    public void setExam_date (String exam_date)
    {
        this.exam_date = exam_date;
    }

    public String getScreening_site ()
    {
        return screening_site;
    }

    public void setScreening_site (String screening_site)
    {
        this.screening_site = screening_site;
    }

    public String getPanel_physician ()
    {
        return panel_physician;
    }

    public void setPanel_physician (String panel_physician)
    {
        this.panel_physician = panel_physician;
    }

    public String getRadiology_services ()
    {
        return radiology_services;
    }

    public void setRadiology_services (String radiology_services)
    {
        this.radiology_services = radiology_services;
    }

    public String getLab_tb ()
    {
        return lab_tb;
    }

    public void setLab_tb (String lab_tb)
    {
        this.lab_tb = lab_tb;
    }

    public String getPatient_id ()
    {
        return patient_id;
    }

    public void setPatient_id (String patient_id)
    {
        this.patient_id = patient_id;
    }

    public String getExam_time ()
    {
        return exam_time;
    }

    public void setExam_time (String exam_time)
    {
        this.exam_time = exam_time;
    }

    public String getExam_exp_date ()
    {
        return exam_exp_date;
    }

    public void setExam_exp_date (String exam_exp_date)
    {
        this.exam_exp_date = exam_exp_date;
    }

    @Override
    public String toString() {
        return "Examination{" +
                "exam_place_city='" + exam_place_city + '\'' +
                ", exam_exp_indicator='" + exam_exp_indicator + '\'' +
                ", lab_syphillis='" + lab_syphillis + '\'' +
                ", exam_place_country='" + exam_place_country + '\'' +
                ", exam_date='" + exam_date + '\'' +
                ", screening_site='" + screening_site + '\'' +
                ", panel_physician='" + panel_physician + '\'' +
                ", radiology_services='" + radiology_services + '\'' +
                ", lab_tb='" + lab_tb + '\'' +
                ", patient_id='" + patient_id + '\'' +
                ", exam_time='" + exam_time + '\'' +
                ", exam_exp_date='" + exam_exp_date + '\'' +
                '}';
    }
}
