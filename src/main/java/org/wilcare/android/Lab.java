package org.wilcare.android;

/**
 * Created by qasim on 1/17/15.
 */
public class Lab
{
    private String lab_name;

    private String lab_id;

    public String getLab_name ()
    {
        return lab_name;
    }

    public void setLab_name (String lab_name)
    {
        this.lab_name = lab_name;
    }

    public String getLab_id ()
    {
        return lab_id;
    }

    public void setLab_id (String lab_id)
    {
        this.lab_id = lab_id;
    }

    @Override
    public String toString() {
        return "Lab{" +
                "lab_name='" + lab_name + '\'' +
                ", lab_id='" + lab_id + '\'' +
                '}';
    }
}
