package org.wilcare.android;

/**
 * Created by qasim on 1/17/15.
 */
public class Patient
{
    private String birth_country;

    private String visa_type_code;

    private String personal_information;

    private String tb_status;

    private String insert_at;

    private String respiratory_rate;

    private String right_eye;

    private String first_name;

    private String nic_image;

    private String time;

    private String height;

    private String usa_intended_state;

    private String old_patient_id;

    private String alien_number;

    private String signature;

    private String Chest_X_rays;

    private String previous_medical_num;

    private String xray_date_time;

    private String birth_date;

    private String usa_intended_postal_code;

    private String update_at;

    private String present_city;

    private String is_healthcare;

    private String current_country;

    private String systolic_bp;

    private String lab_code;

    private String prior_country;

    private String Physical_Examination;

    private String is_exam;

    private String receipt_status;

    private String heart_rate;

    private String usa_intendend_address;

    private String email;

    private String computer_no;

    private String last_name;

    private String passport_number;

    private String Laboratory_Test;

    private String Current_Medicine;

    private String sex;

    private String weight;

    private String present_postal_code;

    private String other_image;

    private String usa_intended_city;

    private String us_consolate_country;

    private String uncor_r20;

    private String birth_city;

    private String diastolic_bp;

    private String is_result;

    private String us_consolate_city;

    private String primary_picture;

    private String mi;

    private String Medicine_Refer;

    private String Medical_History;

    private String passport_image;

    private String present_address;

    private String is_print_status;

    private String Vaccination;

    private String left_eye;

    private String is_final_approval;

    private String TST_Test;

    private String patient_id;

    private String uncor_l20;

    private String cor_l20;

    private String cor_r20;

    public String getBirth_country ()
    {
        return birth_country;
    }

    public void setBirth_country (String birth_country)
    {
        this.birth_country = birth_country;
    }

    public String getVisa_type_code ()
    {
        return visa_type_code;
    }

    public void setVisa_type_code (String visa_type_code)
    {
        this.visa_type_code = visa_type_code;
    }

    public String getPersonal_information ()
    {
        return personal_information;
    }

    public void setPersonal_information (String personal_information)
    {
        this.personal_information = personal_information;
    }

    public String getTb_status ()
    {
        return tb_status;
    }

    public void setTb_status (String tb_status)
    {
        this.tb_status = tb_status;
    }

    public String getInsert_at ()
    {
        return insert_at;
    }

    public void setInsert_at (String insert_at)
    {
        this.insert_at = insert_at;
    }

    public String getRespiratory_rate ()
    {
        return respiratory_rate;
    }

    public void setRespiratory_rate (String respiratory_rate)
    {
        this.respiratory_rate = respiratory_rate;
    }

    public String getRight_eye ()
    {
        return right_eye;
    }

    public void setRight_eye (String right_eye)
    {
        this.right_eye = right_eye;
    }

    public String getFirst_name ()
    {
        return first_name;
    }

    public void setFirst_name (String first_name)
    {
        this.first_name = first_name;
    }

    public String getNic_image ()
    {
        return nic_image;
    }

    public void setNic_image (String nic_image)
    {
        this.nic_image = nic_image;
    }

    public String getTime ()
    {
        return time;
    }

    public void setTime (String time)
    {
        this.time = time;
    }

    public String getHeight ()
    {
        return height;
    }

    public void setHeight (String height)
    {
        this.height = height;
    }

    public String getUsa_intended_state ()
    {
        return usa_intended_state;
    }

    public void setUsa_intended_state (String usa_intended_state)
    {
        this.usa_intended_state = usa_intended_state;
    }

    public String getOld_patient_id ()
    {
        return old_patient_id;
    }

    public void setOld_patient_id (String old_patient_id)
    {
        this.old_patient_id = old_patient_id;
    }

    public String getAlien_number ()
    {
        return alien_number;
    }

    public void setAlien_number (String alien_number)
    {
        this.alien_number = alien_number;
    }

    public String getSignature ()
    {
        return signature;
    }

    public void setSignature (String signature)
    {
        this.signature = signature;
    }

    public String getChest_X_rays ()
    {
        return Chest_X_rays;
    }

    public void setChest_X_rays (String Chest_X_rays)
    {
        this.Chest_X_rays = Chest_X_rays;
    }

    public String getPrevious_medical_num ()
    {
        return previous_medical_num;
    }

    public void setPrevious_medical_num (String previous_medical_num)
    {
        this.previous_medical_num = previous_medical_num;
    }

    public String getXray_date_time ()
    {
        return xray_date_time;
    }

    public void setXray_date_time (String xray_date_time)
    {
        this.xray_date_time = xray_date_time;
    }

    public String getBirth_date ()
    {
        return birth_date;
    }

    public void setBirth_date (String birth_date)
    {
        this.birth_date = birth_date;
    }

    public String getUsa_intended_postal_code ()
    {
        return usa_intended_postal_code;
    }

    public void setUsa_intended_postal_code (String usa_intended_postal_code)
    {
        this.usa_intended_postal_code = usa_intended_postal_code;
    }

    public String getUpdate_at ()
    {
        return update_at;
    }

    public void setUpdate_at (String update_at)
    {
        this.update_at = update_at;
    }

    public String getPresent_city ()
    {
        return present_city;
    }

    public void setPresent_city (String present_city)
    {
        this.present_city = present_city;
    }

    public String getIs_healthcare ()
    {
        return is_healthcare;
    }

    public void setIs_healthcare (String is_healthcare)
    {
        this.is_healthcare = is_healthcare;
    }

    public String getCurrent_country ()
    {
        return current_country;
    }

    public void setCurrent_country (String current_country)
    {
        this.current_country = current_country;
    }

    public String getSystolic_bp ()
    {
        return systolic_bp;
    }

    public void setSystolic_bp (String systolic_bp)
    {
        this.systolic_bp = systolic_bp;
    }

    public String getLab_code ()
    {
        return lab_code;
    }

    public void setLab_code (String lab_code)
    {
        this.lab_code = lab_code;
    }

    public String getPrior_country ()
    {
        return prior_country;
    }

    public void setPrior_country (String prior_country)
    {
        this.prior_country = prior_country;
    }

    public String getPhysical_Examination ()
    {
        return Physical_Examination;
    }

    public void setPhysical_Examination (String Physical_Examination)
    {
        this.Physical_Examination = Physical_Examination;
    }

    public String getIs_exam ()
    {
        return is_exam;
    }

    public void setIs_exam (String is_exam)
    {
        this.is_exam = is_exam;
    }

    public String getReceipt_status ()
    {
        return receipt_status;
    }

    public void setReceipt_status (String receipt_status)
    {
        this.receipt_status = receipt_status;
    }

    public String getHeart_rate ()
    {
        return heart_rate;
    }

    public void setHeart_rate (String heart_rate)
    {
        this.heart_rate = heart_rate;
    }

    public String getUsa_intendend_address ()
    {
        return usa_intendend_address;
    }

    public void setUsa_intendend_address (String usa_intendend_address)
    {
        this.usa_intendend_address = usa_intendend_address;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getComputer_no ()
    {
        return computer_no;
    }

    public void setComputer_no (String computer_no)
    {
        this.computer_no = computer_no;
    }

    public String getLast_name ()
    {
        return last_name;
    }

    public void setLast_name (String last_name)
    {
        this.last_name = last_name;
    }

    public String getPassport_number ()
    {
        return passport_number;
    }

    public void setPassport_number (String passport_number)
    {
        this.passport_number = passport_number;
    }

    public String getLaboratory_Test ()
    {
        return Laboratory_Test;
    }

    public void setLaboratory_Test (String Laboratory_Test)
    {
        this.Laboratory_Test = Laboratory_Test;
    }

    public String getCurrent_Medicine ()
    {
        return Current_Medicine;
    }

    public void setCurrent_Medicine (String Current_Medicine)
    {
        this.Current_Medicine = Current_Medicine;
    }

    public String getSex ()
    {
        return sex;
    }

    public void setSex (String sex)
    {
        this.sex = sex;
    }

    public String getWeight ()
    {
        return weight;
    }

    public void setWeight (String weight)
    {
        this.weight = weight;
    }

    public String getPresent_postal_code ()
    {
        return present_postal_code;
    }

    public void setPresent_postal_code (String present_postal_code)
    {
        this.present_postal_code = present_postal_code;
    }

    public String getOther_image ()
    {
        return other_image;
    }

    public void setOther_image (String other_image)
    {
        this.other_image = other_image;
    }

    public String getUsa_intended_city ()
    {
        return usa_intended_city;
    }

    public void setUsa_intended_city (String usa_intended_city)
    {
        this.usa_intended_city = usa_intended_city;
    }

    public String getUs_consolate_country ()
    {
        return us_consolate_country;
    }

    public void setUs_consolate_country (String us_consolate_country)
    {
        this.us_consolate_country = us_consolate_country;
    }

    public String getUncor_r20 ()
    {
        return uncor_r20;
    }

    public void setUncor_r20 (String uncor_r20)
    {
        this.uncor_r20 = uncor_r20;
    }

    public String getBirth_city ()
    {
        return birth_city;
    }

    public void setBirth_city (String birth_city)
    {
        this.birth_city = birth_city;
    }

    public String getDiastolic_bp ()
    {
        return diastolic_bp;
    }

    public void setDiastolic_bp (String diastolic_bp)
    {
        this.diastolic_bp = diastolic_bp;
    }

    public String getIs_result ()
    {
        return is_result;
    }

    public void setIs_result (String is_result)
    {
        this.is_result = is_result;
    }

    public String getUs_consolate_city ()
    {
        return us_consolate_city;
    }

    public void setUs_consolate_city (String us_consolate_city)
    {
        this.us_consolate_city = us_consolate_city;
    }

    public String getPrimary_picture ()
    {
        return primary_picture;
    }

    public void setPrimary_picture (String primary_picture)
    {
        this.primary_picture = primary_picture;
    }

    public String getMi ()
    {
        return mi;
    }

    public void setMi (String mi)
    {
        this.mi = mi;
    }

    public String getMedicine_Refer ()
    {
        return Medicine_Refer;
    }

    public void setMedicine_Refer (String Medicine_Refer)
    {
        this.Medicine_Refer = Medicine_Refer;
    }

    public String getMedical_History ()
    {
        return Medical_History;
    }

    public void setMedical_History (String Medical_History)
    {
        this.Medical_History = Medical_History;
    }

    public String getPassport_image ()
    {
        return passport_image;
    }

    public void setPassport_image (String passport_image)
    {
        this.passport_image = passport_image;
    }

    public String getPresent_address ()
    {
        return present_address;
    }

    public void setPresent_address (String present_address)
    {
        this.present_address = present_address;
    }

    public String getIs_print_status ()
    {
        return is_print_status;
    }

    public void setIs_print_status (String is_print_status)
    {
        this.is_print_status = is_print_status;
    }

    public String getVaccination ()
    {
        return Vaccination;
    }

    public void setVaccination (String Vaccination)
    {
        this.Vaccination = Vaccination;
    }

    public String getLeft_eye ()
    {
        return left_eye;
    }

    public void setLeft_eye (String left_eye)
    {
        this.left_eye = left_eye;
    }

    public String getIs_final_approval ()
    {
        return is_final_approval;
    }

    public void setIs_final_approval (String is_final_approval)
    {
        this.is_final_approval = is_final_approval;
    }

    public String getTST_Test ()
    {
        return TST_Test;
    }

    public void setTST_Test (String TST_Test)
    {
        this.TST_Test = TST_Test;
    }

    public String getPatient_id ()
    {
        return patient_id;
    }

    public void setPatient_id (String patient_id)
    {
        this.patient_id = patient_id;
    }

    public String getUncor_l20 ()
    {
        return uncor_l20;
    }

    public void setUncor_l20 (String uncor_l20)
    {
        this.uncor_l20 = uncor_l20;
    }

    public String getCor_l20 ()
    {
        return cor_l20;
    }

    public void setCor_l20 (String cor_l20)
    {
        this.cor_l20 = cor_l20;
    }

    public String getCor_r20 ()
    {
        return cor_r20;
    }

    public void setCor_r20 (String cor_r20)
    {
        this.cor_r20 = cor_r20;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "birth_country='" + birth_country + '\'' +
                ", visa_type_code='" + visa_type_code + '\'' +
                ", personal_information='" + personal_information + '\'' +
                ", tb_status='" + tb_status + '\'' +
                ", insert_at='" + insert_at + '\'' +
                ", respiratory_rate='" + respiratory_rate + '\'' +
                ", right_eye='" + right_eye + '\'' +
                ", first_name='" + first_name + '\'' +
                ", nic_image='" + nic_image + '\'' +
                ", time='" + time + '\'' +
                ", height='" + height + '\'' +
                ", usa_intended_state='" + usa_intended_state + '\'' +
                ", old_patient_id='" + old_patient_id + '\'' +
                ", alien_number='" + alien_number + '\'' +
                ", signature='" + signature + '\'' +
                ", Chest_X_rays='" + Chest_X_rays + '\'' +
                ", previous_medical_num='" + previous_medical_num + '\'' +
                ", xray_date_time='" + xray_date_time + '\'' +
                ", birth_date='" + birth_date + '\'' +
                ", usa_intended_postal_code='" + usa_intended_postal_code + '\'' +
                ", update_at='" + update_at + '\'' +
                ", present_city='" + present_city + '\'' +
                ", is_healthcare='" + is_healthcare + '\'' +
                ", current_country='" + current_country + '\'' +
                ", systolic_bp='" + systolic_bp + '\'' +
                ", lab_code='" + lab_code + '\'' +
                ", prior_country='" + prior_country + '\'' +
                ", Physical_Examination='" + Physical_Examination + '\'' +
                ", is_exam='" + is_exam + '\'' +
                ", receipt_status='" + receipt_status + '\'' +
                ", heart_rate='" + heart_rate + '\'' +
                ", usa_intendend_address='" + usa_intendend_address + '\'' +
                ", email='" + email + '\'' +
                ", computer_no='" + computer_no + '\'' +
                ", last_name='" + last_name + '\'' +
                ", passport_number='" + passport_number + '\'' +
                ", Laboratory_Test='" + Laboratory_Test + '\'' +
                ", Current_Medicine='" + Current_Medicine + '\'' +
                ", sex='" + sex + '\'' +
                ", weight='" + weight + '\'' +
                ", present_postal_code='" + present_postal_code + '\'' +
                ", other_image='" + other_image + '\'' +
                ", usa_intended_city='" + usa_intended_city + '\'' +
                ", us_consolate_country='" + us_consolate_country + '\'' +
                ", uncor_r20='" + uncor_r20 + '\'' +
                ", birth_city='" + birth_city + '\'' +
                ", diastolic_bp='" + diastolic_bp + '\'' +
                ", is_result='" + is_result + '\'' +
                ", us_consolate_city='" + us_consolate_city + '\'' +
                ", primary_picture='" + primary_picture + '\'' +
                ", mi='" + mi + '\'' +
                ", Medicine_Refer='" + Medicine_Refer + '\'' +
                ", Medical_History='" + Medical_History + '\'' +
                ", passport_image='" + passport_image + '\'' +
                ", present_address='" + present_address + '\'' +
                ", is_print_status='" + is_print_status + '\'' +
                ", Vaccination='" + Vaccination + '\'' +
                ", left_eye='" + left_eye + '\'' +
                ", is_final_approval='" + is_final_approval + '\'' +
                ", TST_Test='" + TST_Test + '\'' +
                ", patient_id='" + patient_id + '\'' +
                ", uncor_l20='" + uncor_l20 + '\'' +
                ", cor_l20='" + cor_l20 + '\'' +
                ", cor_r20='" + cor_r20 + '\'' +
                '}';
    }
}