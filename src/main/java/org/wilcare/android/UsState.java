package org.wilcare.android;

/**
 * Created by qasim on 1/17/15.
 */
public class UsState
{
    private String state_id;

    private String us_state;

    private String country_id;

    private String state_name;

    public String getState_id ()
    {
        return state_id;
    }

    public void setState_id (String state_id)
    {
        this.state_id = state_id;
    }

    public String getUs_state ()
    {
        return us_state;
    }

    public void setUs_state (String us_state)
    {
        this.us_state = us_state;
    }

    public String getCountry_id ()
    {
        return country_id;
    }

    public void setCountry_id (String country_id)
    {
        this.country_id = country_id;
    }

    public String getState_name ()
    {
        return state_name;
    }

    public void setState_name (String state_name)
    {
        this.state_name = state_name;
    }

    @Override
    public String toString() {
        return "UsState{" +
                "state_id='" + state_id + '\'' +
                ", us_state='" + us_state + '\'' +
                ", country_id='" + country_id + '\'' +
                ", state_name='" + state_name + '\'' +
                '}';
    }
}
