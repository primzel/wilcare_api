import org.wilcare.Constents;
import org.wilcare.android.*;
import org.wilcare.iclient.CountryClient;
import org.wilcare.iclient.ExamClient;
import org.wilcare.iclient.LabClient;
import org.wilcare.iclient.PatientClient;
import retrofit.RestAdapter;

import java.util.List;

public class Main {
    private static RestAdapter adapter=new RestAdapter.Builder().setEndpoint(Constents.URL).setLogLevel(RestAdapter.LogLevel.FULL).build();
    public static void main(String[] args) {

        CountryClient client=adapter.create(CountryClient.class);

        List<CountryData> list=client.getCountryData();
        for (CountryData p : list){
            System.out.println(p);
        }


    }
}
